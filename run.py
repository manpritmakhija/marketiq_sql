import os.path
from os import listdir
import mysql.connector
from os.path import isfile, join

# Path to root directory (separate with backslash)
mypath = 'C:\marketiq'

# Depth of root folder
ind = len(mypath.split("\\"))

# Connecting to SQL server
mydb = mysql.connector.connect(
  host="",
  user="",
  password="",
  database = ""
)

# Creating Table as STRUCTURE
mycursor = mydb.cursor()
mycursor.execute("CREATE TABLE STRUCTURE (location VARCHAR(200), client VARCHAR(200), subtopic VARCHAR(200), path text, file VARCHAR(200))")

n= 0
def rec(path,n):

    # Counter to track the depth of traversal.
    n+=1

    # Condition of reaching to the bottom folder
    # This condition will trigger the insertion process as we reach to the folder containing required files.
    if n == 4:
        for i in listdir(path):

            file_info = path.split("\\")
            loc = file_info[ind]
            client = file_info[ind+1]
            sub_topic = file_info[ind+2]
            path3 = os.path.join(path,i)
            file = i
            # Inserting values to sql table
            sql = "INSERT INTO STRUCTURE (location, client, subtopic, path, file) VALUES (%s, %s, %s, %s, %s)"
            val = (loc, client, sub_topic, path3, file)
            mycursor.execute(sql, val)
            mydb.commit()
            print(mycursor.rowcount, "details inserted")
        return

    # Traversing through the subdirectories present inside path folder
    for f in [f for f in listdir(path) if os.path.isdir(join(path, f))]:
        path2 = os.path.join(path,f)
        # Recursively traversing across the depth of folders with each subdirectory acting as path for...
        # ... the next part of recursion.
        rec(path2,n)

rec(mypath,n)

mydb.close()